function Network()
{
    var items = Array.prototype.slice.call(arguments);

    /*Class - items of network*/
    Network.ELECTRIC_STATION = function(s){
        this.MIN_ENERGY_DAY = 1;
        this.energy_day = s;
        this.MAX_ENERGY_DAY = 100;
        this.energy_night = s;
        this.TYPE = "PRODUCE";
    };

    Network.SUN_PANEL = function(s){
        this.MIN_ENERGY_DAY = 1;
        this.energy_day = s;
        this.MAX_ENERGY_DAY = 5;
        this.energy_night = 0;
        this.TYPE = "PRODUCE";
    };

    Network.HOME = function(appartments){
        this.ENERGY_DAY = 4;
        this.ENERGY_NIGHT = 1;
        this.COUNT = appartments;
        this.TYPE = "USE";
    };

    Network.ELECTRIC_LINE = function(energy, price){
        this.energy = energy;
        this.price = price;
        this.TYPE = "LINE";
    };
    /*END*/

    /*Hidden class*/
    var information = new function(){
        var self = this;
        this.produce_energy_day = 0;
        this.use_energy_day = 0;
        this.produce_energy_night = 0;
        this.use_energy_night = 0;
        this.clear = function () {
            self.produce_energy_day = 0;
            self.use_energy_day = 0;
            self.produce_energy_night = 0;
            self.use_energy_night = 0;
        };
    };

    var result = new function(){
        var self = this;
        this.price_day = 0;
        this.pay_day = true;
        this.price_night = 0;
        this.pay_night = true;
        this.clear = function(){
            self.price_day = 0;
            self.price_day = true;
            self.price_night = 0;
            self.pay_night = true;
        };
    };
    /*END*/

    /*Help variables*/
    var max_price_line = new Network.ELECTRIC_LINE(0, 0);
    var min_price_line = new Network.ELECTRIC_LINE(0, 0);
    var result_max_price_line = new Network.ELECTRIC_LINE(0, 0);
    var result_min_price_line = new Network.ELECTRIC_LINE(0, 0);
    /*END*/

    /*Helping functions*/
    function ComputeResult() {
        information.clear();
        result.clear();
        result_max_price_line = new Network.ELECTRIC_LINE(0, 0);
        result_min_price_line = new Network.ELECTRIC_LINE(0, 0);

        var items1 = [];
        items1 = items1.concat(items);

        items.forEach(AddToInformation);

        for (var i = 0; i < items.length; i++) {
            for(var j = 0; j < items.length; j++){
                AddMinMaxLines(items[j]);
            }
            if (result_max_price_line.energy < Math.abs(information.use_energy_day - information.produce_energy_day)) {
                result_max_price_line.energy += max_price_line.energy;
                result_max_price_line.price += max_price_line.price;
            } else {
                if (result_min_price_line.energy < Math.abs(information.use_energy_day - information.produce_energy_day)) {
                    result_min_price_line.energy += min_price_line.energy;
                    result_min_price_line.price += min_price_line.price;
                }
            }
        }

        items = [];
        items = items.concat(items1);

        if (result_max_price_line.energy < information.use_energy_day - information.produce_energy_day &&
            result_min_price_line.energy < information.use_energy_day - information.produce_energy_day) {
            throw new Error("Лінії не проводять потрібну нам кількість енергії! Неможливо живити місто!");
        }

        AddToResult();
    }

    function AddToResult(){ //Compute information to result class
        if(information.produce_energy_day > information.use_energy_day)
        {
            result.price_day = (information.produce_energy_day - information.use_energy_day) % result_max_price_line.energy * result_max_price_line.price;
            result.pay_day = false;
        }else{
            result.price_day = (information.use_energy_day - information.produce_energy_day) % result_min_price_line.energy * result_min_price_line.price;
            result.pay_day = true;
        }

        if(information.produce_energy_night > information.use_energy_night)
        {
            result.price_night = (information.produce_energy_night - information.use_energy_night) % result_max_price_line.energy * result_max_price_line.price;
            result.pay_night = false;
        }else{
            result.price_night = (information.use_energy_night - information.produce_energy_night) % result_min_price_line.energy * result_min_price_line.price;
            result.pay_night = true;
        }
    }

    function AddToInformation(item){ //Add information to information class
        if(item.TYPE == "PRODUCE") {
            if(item.energy_day < item.MIN_ENERGY_DAY || item.energy_day > item.MAX_ENERGY_DAY)
                throw new Error("Кількість виробленої енергії виходить за допустимі межі.");
            information.produce_energy_day += item.energy_day;
            information.produce_energy_night += item.energy_night;
        }else if(item.TYPE == "USE"){
            if(item.COUNT > 400 || item.COUNT < 0)
                throw new Error("Кількість будинків в домі виходять за допустимі межі.");
            information.use_energy_day = item.ENERGY_DAY * item.COUNT;
            information.use_energy_night = item.ENERGY_NIGHT * item.COUNT;
        }
    }

    function AddMinMaxLines(item){ //Add min and max network lines to helping variables
        if(item.TYPE == "LINE") {
            if (item.price == max_price_line.price && item.energy > max_price_line.energy) {
                max_price_line = item;
            }
            if (item.price == min_price_line.price && item.energy > max_price_line.energy) {
                min_price_line = item;
            }
            if (item.price > max_price_line.price) {
                max_price_line = item;
            }
            if ((item.price < min_price_line.price && item.price > 0) || min_price_line.price == 0) {
                min_price_line = item;
            }
        }else
            if(item.TYPE != "USE" && item.TYPE != "PRODUCE")
            {
                throw new Error("Невірний елемент мережі: " + item.TYPE);
            }
    }
    /*END*/

    /*Methods*/
    this.getResult = function(){
        ComputeResult();

        return result;
    };

    this.getItems = function(){
        return items;
    };

    this.addItems = function()
    {
        var args1 = Array.prototype.slice.call(arguments);
        if(args1.length == 0){
            throw new Error("Нічого додавати!");
        }
        items = items.concat(args1);
    };

    this.getPriceForDay = function(){
        return (result.pay_day) ? -1 * result.price_day : result.price_day;
    };

    this.getPriceForNight = function(){
        return (result.pay_night) ? -1 * result.price_night : result.price_night;
    };
    /*END*/
}

new Network();