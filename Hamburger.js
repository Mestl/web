function Hamburger() {

    /*Скрытые дополнительные поля / переменные*/

    var is_one_hamburger = 0;
    var self = this;
    var args = Array.prototype.slice.call(arguments);

    /*END*/

    /*Константные классы*/

    Hamburger.BIG = new function () {
        this.NAME = "BIG HAMBURGER";
        this.PRICE = 100;
        this.CALORIES = 40;
        this.TYPE = 'HAMBURGER';
    };

    Hamburger.SMALL = new function () {
        this.NAME = "SMALL HAMBURGER";
        this.PRICE = 50;
        this.CALORIES = 20;
        this.TYPE = 'HAMBURGER';
    };

    Hamburger.POTATO = new function () {
        this.NAME = "POTATO";
        this.PRICE = 15;
        this.CALORIES = 10;
        this.TYPE = 'STUFFING';
    };

    Hamburger.SALAD = new function () {
        this.NAME = "SALAD";
        this.PRICE = 20;
        this.CALORIES = 5;
        this.TYPE = 'STUFFING';
    };

    Hamburger.CHEESE = new function () {
        this.NAME = "CHEESE";
        this.PRICE = 10;
        this.CALORIES = 20;
        this.TYPE = 'STUFFING';
    };

    Hamburger.MAYONNAISE = new function () {
        this.NAME = "MAYONNAISE";
        this.PRICE = 20;
        this.CALORIES = 5;
        this.TYPE = 'TOPPING';
    };

    Hamburger.SEASONING = new function () {
        this.NAME = "SEASONING";
        this.PRICE = 15;
        this.CALORIES = 0;
        this.TYPE = 'TOPPING';
    };

    /*END*/

    /*Класс результата*/

    var result = new function () {
        this.price = 0;
        this.calories = 0;
        var me = this;
        this.clear = function(){
            me.price = 0;
            me.calories = 0;
        };
    };

    /*END*/

    /*Скрытые классы обсчетов*/

    var computeResult = function () {
        args.forEach(addToResult);
    };

    var addToResult = function (obj, index, array) {
        try {
            result.price += obj.PRICE;
            result.calories += obj.CALORIES;
            if (obj.TYPE == "HAMBURGER") {
                is_one_hamburger++;
            }
        } catch (err) {
            throw new SyntaxError("Hamburger exeption: " + "Не вірно задан тип гамбургера!");
        }
    };

    /*END*/

    /*Доступные методы класса*/

    this.addItems = function()
    {
        var args1 = Array.prototype.slice.call(arguments);
        if(args1.length == 0){
            throw new Error("Нічого додавати!");
        }
        args = args.concat(args1);
    };

    this.getResult = function () {
        result.clear();
        is_one_hamburger = 0;
        computeResult();
        switch (is_one_hamburger) {
            case 0:
                throw new Error("Не обрано гамбургер!");
                break;
            case 1:
                return result;
                break;
            default:
                throw new Error("Забагато гамбургерів!");
                break;
        }
    };

    this.getItems = function(){
        return args;
    };

    this.getCalories = function(){
        var res = self.getResult();
        return res.calories;
    };

    this.getPrice = function(){
        var res = self.getResult();
        return res.price;
    };

    /*END*/
}
new Hamburger();
