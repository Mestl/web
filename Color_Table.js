/**
 * Created by bogda on 24.03.2016.
 */
function Table(height, width){
    var Height = height;
    var Width = width;
    var PositionX = 0;
    var PositionY = 0;
    var table;

    this.setCoords = function(x, y){
        PositionX = x;
        PositionY = y;
    };

    this.getElementTable = function(){
        return document.getElementById('saper_table');
    };

    this.createTable = function(BlockId){

        document.getElementById(BlockId).innerHTML = '';

        table = document.createElement('table');

        table.setAttribute('id', 'saper_table');
        table.setAttribute('width', (Width*28).toString() + 'px');
        table.setAttribute('height', (Height*28).toString() + 'px');
        table.setAttribute('position', 'relative');
        table.setAttribute('left', PositionX + 'px');
        table.setAttribute('top', PositionY + 'px');
        table.setAttribute('border', '1px');
        table.style.mozUserSelect = '~moz~none';
        table.style.webkitUserSelect = 'none';
        table.style.userSelect = 'none';
        table.style.borderCollapse = 'collapse';
        table.style.backgroundColor = '';

        for(var i = 0; i < Height; i++)
        {
            var tr = table.insertRow(-1);

            tr.style.mozUserSelect = '~moz~none';
            tr.style.webkitUserSelect = 'none';
            tr.style.userSelect = 'none';

            for(var j = 0; j < Width; j++)
            {
                tr.insertCell(-1);
                var span = document.createElement('span');
                span.innerHTML = "0";
                tr.lastChild.appendChild(span);
                tr.lastChild.classList.add('number');
            }
        }

        document.getElementById(BlockId).appendChild(table);
    };
}

function Saper(TableId, width, height, difficult){
    var table = new Table(height, width);
    table.createTable('table');

    table = table.getElementTable();

    var countMines = parseInt((height * width) * difficult / 15);
    var countFlagsOnMines = 0;

    table.firstElementChild.style.mozUserSelect = '~moz~none';
    table.firstElementChild.style.webkitUserSelect = 'none';
    table.firstElementChild.style.userSelect = 'none';
    table.style.mozUserSelect = '~moz~none';
    table.style.webkitUserSelect = 'none';
    table.style.userSelect = 'none';


    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function initialize_mines(){
        for(var i = 0; i < countMines; i++)
        {
            var y = getRandomInt(1, height + 1);
            var x = getRandomInt(1, width + 1);
            var em = table.querySelector('tr:nth-child('+y+') td:nth-child('+x+')');
            if( !em.classList.contains('bomb') )
            {
                setClassBomb(em);
                em.classList.remove('number');
                setNumbersAround(em);
            }
        }
    }

    function setNumbersAround(target){
        var y = target.parentElement.rowIndex + 1;
        var x = target.cellIndex + 1;
        for(var i = x - 1; i <= x + 1; i++)
            for(var j = y - 1; j <= y + 1; j++)
                if(i > 0 && i < width + 1 && j > 0 && j < height + 1) {
                    var em = table.querySelector('tr:nth-child('+j+') td:nth-child('+i+')');
                    var span = em.firstElementChild;
                    if(em.classList.contains('bomb'))
                    {
                        continue;
                    }
                    var num = parseInt(span.innerHTML);
                    num++;
                    span.innerHTML = num;
                }
    }

    function showZeroAroundNumber(target){
        var y = target.parentElement.rowIndex + 1;
        var x = target.cellIndex + 1;
        for(var i = x - 1; i <= x + 1; i++)
            for(var j = y - 1; j <= y + 1; j++)
                if(i > 0 && i < width + 1 && j > 0 && j < height + 1) {
                    var em = table.querySelector('tr:nth-child('+j+') td:nth-child('+i+')');

                    var span = em.firstElementChild;
                    if(em.classList.contains('bomb') || em.classList.contains('show') || em.classList.contains('flag'))
                    {
                        continue;
                    }

                    em.classList.add('show');
                    if(em.lastChild.innerHTML == "0")
                        showZeroAroundNumber(em);
                }
    }

    function setClassBomb(target){
        var span = target.firstElementChild;
        span.innerHTML = '';
        if (target.classList.contains('bomb'))
            target.classList.remove('bomb');
        else
            target.classList.add('bomb');
    }

    function showResult(message){
        var a = document.getElementById('template-popup');
        var src = a.innerHTML;

        var compiled = Handlebars.compile(src);

        var result = compiled({
            text: message
        });

        a = document.getElementById('table');
        a.innerHTML += result;


        var w = document.getElementById('width');
        var h = document.getElementById('height');
        var d = document.getElementById('difficult');

        console.log(width + " " + height + " " + difficult);

        w.value = width;
        h.value = height;
        d.value = difficult;

        var button = document.getElementById('new-game');
        console.log(button);
        button.onclick = function(){

            w = parseInt(w.value);
            h = parseInt(h.value);
            d = parseInt(d.value);

            console.log(d);

            var aa = a.lastChild;
            a.removeChild(aa);


            new Saper('saper_table', w, h, d);
        };
    }

    function setClassFlag(target){
        if (target.classList.contains('flag')) {
            if(target.classList.contains('bomb'))
                countFlagsOnMines--;
            target.classList.remove('flag');
        }
        else {
            if(target.classList.contains('bomb'))
                countFlagsOnMines++;
            play_sound(Saper.PUT_ATENTION);
            target.classList.add('flag');
        }
        console.log(countMines);
        console.log(countFlagsOnMines);
        if(countFlagsOnMines >= countMines)
        {
            !table.classList.contains('show') ? table.classList.add('show') : target ;
            !table.classList.contains('win') ? table.classList.add('win') : target ;

            showResult('YOU WIN!');

            play_sound(Saper.VICTORY);

        }
    }

    table.oncontextmenu = function(event){
        var target = event.target;
        if(target != table) {
            setClassFlag(target);
            event.preventDefault();
        }
        return false;
    };

    table.onclick = function(event){
        var target = event.target;
        if(target != table) {
            event.preventDefault();
            if (!target.classList.contains('bomb'))
            {
                play_sound(Saper.OPEN_NUMBER);
                if(target.lastChild.innerHTML == "0")
                    showZeroAroundNumber(target);
                !target.classList.contains('show') ? target.classList.add('show') : target ;
                !target.classList.contains('number') ? target.classList.add('number') : target;
            }
            else
            {
                play_sound(Saper.BOOM);
                !table.classList.contains('lose') ? table.classList.add('lose') : target ;
                !table.classList.contains('show') ? table.classList.add('show') : target ;

                showResult('GAME OVER');
            }
        }
    };

    Saper.BOOM = "sound/boom.mp3";
    Saper.OPEN_NUMBER = "sound/open.mp3";
    Saper.PUT_ATENTION = "sound/attention.mp3";
    Saper.VICTORY = "sound/victory.mp3";
    Saper.LOSE = "sound/lose.mp3";

    function play_sound(path){
            console.log(path);
            var audio = document.getElementById('my_audio');
            audio.src = path;
            audio.autoplay = true;
    }
    var audio = document.createElement('audio');
    audio.id = 'my_audio';
    table.parentElement.appendChild(audio);
    initialize_mines();
}


new Saper('saper_table', 10, 10, 4);